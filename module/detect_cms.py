#!/usr/bin/env python3
import requests
from bs4 import BeautifulSoup

#  pylint: disable=W0312


def detect_cms(url, src, robots, path, logs):
	""" Determine which CMS is used by the website """
	# https://websitesetup.org/popular-cms/

	print("")
	print("Trying to detect possible CMS ...")

	if logs:
		file = open(f"{path}", "a")

	# Wordpress
	if "wordpress" in src.lower() or "wp-" in src.lower():
		print("CMS: Wordpress")
		wordpress_version(url, path, logs)
	else:
		try:
			if requests.get(url+"/wp-content/").text == "":
				print("CMS: Wordpress")
				wordpress_version(url, path, logs)
		except requests.exceptions.ConnectionError:
			pass

	# Joomla
	if "joomla" in src.lower() or "joomla" in robots.lower():
		print("CMS: Joomla")
		if logs:
			file.write("CMS: Joomla\n")

	# Drupal
	if "drupal" in src.lower() or "drupal" in robots.lower():
		print("CMS: Drupal")
		drupal_version(url, path, logs)

	# Shopify
	if "shopify" in src.lower() or "shopify" in robots.lower():
		print("CMS: Shopify")
		if logs:
			file.write("CMS: Shopify\n")

	# WooCommerce
	if "woocommerce" in src.lower() or "woocommerce" in robots.lower():
		print("CMS: WooCommerce")
		if logs:
			file.write("CMS: WooCommerce\n")

	# Magento
	if "magento" in src.lower() or "magento" in robots.lower():
		print("CMS: Magento")
		if logs:
			file.write("CMS: Magento\n")

	# E-monsite
	if "e-monsite" in src.lower() or "e-monsite" in robots.lower():
		print("CMS: E-monsite")
		if logs:
			file.write("CMS: E-monsite\n")

	# Blogger
	if "blogger.com" in src.lower() or "blogger.com" in robots.lower():
		print("CMS: Blogger")
		if logs:
			file.write("CMS: Blogger\n")

	# Wix
	if "wix.com" in src.lower() or "wix.com" in robots.lower():
		print("CMS: Wix")
		if logs:
			file.write("CMS: Wix\n")

	if logs:
		file.close()


def wordpress_version(url, path, logs):
	""" Determine the wordpress version """
	req = requests.get(url).text
	try:
		version_generator = BeautifulSoup(req, "html.parser").find("meta", {"name": "generator"})["content"]
	except TypeError:
		version_generator = 0

	versions = []
	hrefs = []
	links = BeautifulSoup(req, "html.parser").find_all("link", {"rel": "stylesheet"})
	for link in links:
		hrefs.append(link["href"])

	for href in hrefs:
		if "?ver=" in href and "jquery" not in href:
			ver = href.split("?ver=")[1]
			if ver not in versions and ver != "1.0.0" and "." in ver:
				versions.append(ver)

	if logs:
		with open(f"{path}", "a") as file:
			file.write("CMS: Wordpress\n")
			if version_generator != 0 or versions:
				print("Wordpress possible version:")
				file.write("Wordpress possible version:\n")
				if version_generator != 0:
					print(f"	Generator: {version_generator}")
					file.write(f"	Generator: {version_generator}\n")
				for version in versions:
					print(f"	JS/CSS/Themes: WordPress {version}")
					file.write(f"	JS/CSS/Themes: WordPress {version}\n")
	else:
		if version_generator != 0 or versions:
			print("Wordpress possible version:")
			if version_generator != 0:
				print(f"	Generator: {version_generator}")
			for version in versions:
				print(f"	JS/CSS/Themes: WordPress {version}")


def drupal_version(url, path, logs):
	""" Determine the drupal version """
	req = requests.get(url).text
	try:
		version_generator = BeautifulSoup(req, "html.parser").find("meta", {"name": "Generator"})["content"]
	except TypeError:
		version_generator = 0

	versions = []
	hrefs = []
	links = BeautifulSoup(req, "html.parser").find_all("link", {"rel": "stylesheet"})
	for link in links:
		hrefs.append(link["href"])

	for href in hrefs:
		if "?v=" in href and "jquery" not in href:
			ver = href.split("?v=")[1]
			if ver not in versions and ver != "1.0.0" and "." in ver:
				versions.append(ver)

	if logs:
		with open(f"{path}", "a") as file:
			file.write("CMS: Drupal\n")
			if version_generator != 0 or versions:
				print("Drupal possible version:")
				file.write("Drupal possible version:\n")
				if version_generator != 0:
					print(f"	Generator: {version_generator.replace(' (https://www.drupal.org)', '')}")
					file.write(f"	Generator: {version_generator.replace(' (https://www.drupal.org)', '')}\n")
				for version in versions:
					print(f"	JS/CSS/Themes: Drupal {version}")
					file.write(f"	JS/CSS/Themes: Drupal {version}\n")
	else:
		if version_generator != 0 or versions:
			print("Drupal possible version:")
			if version_generator != 0:
				print(f"	Generator: {version_generator.replace(' (https://www.drupal.org)', '')}")
			for version in versions:
				print(f"	JS/CSS/Themes: Drupal {version}")
