#!/usr/bin/env python3
import sys
import requests
from module.display import print_red, print_yellow, print_green

def check_data(url, useragent, proxy_dict, logs_path, logs):
    """ Try to find website's sensitive files or directories """

    paths = [".git", ".env", ".setup", ".htpasswd"]
    contains = ["branches", "APP_ENV", "", ""]

    for path, contain in zip(paths, contains):
        print("Checking ... => " + path)
        try:
            req = requests.get(url + "/" + path, headers=useragent, proxies=proxy_dict)
            if req.status_code == 200 and contain in req.text:
                print(f"    {print_red('[!]')} {print_yellow('Warning =>')} {url}/{path}")
            else:
                print(f"    {print_green('Passed !')}")
        except requests.exceptions.ConnectionError:
            print(f"    {print_green('Passed !')}")
    print("")

def check_robots(robot):
    print("Checking for sensitive paths in 'robots.txt' ... ")
    for line in robot.split("\n"):
        if "Disallow: " in line:
            print(line.split("Disallow: ")[1])

    