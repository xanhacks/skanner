#!/usr/bin/env python3
import platform
import os


def display_banner(version):
    """ display the project's banner """
    print(fr"""________________________________________________________
   _____ __                              
  / ___// /______ _____  ____  ___  _____
  \__ \/ //_/ __ `/ __ \/ __ \/ _ \/ ___/
 ___/ / ,< / /_/ / / / / / / /  __/ /    
/____/_/|_|\__,_/_/ /_/_/ /_/\___/_/     

{print_red("Author:")} xanhacks
{print_red("Version:")} {version}
{print_red("Project:")} https://gitlab.com/xanhacks/skanner
________________________________________________________
""")


# Text color
def print_red(text):
    """ print the text in red """
    return f"\033[91m{text}\033[00m"


def print_green(text):
    """ print the text in green """
    return f"\033[92m{text}\033[00m"


def print_yellow(text):
    """ print the text in yellow """
    return f"\033[93m{text}\033[00m"


def print_light_purple(text):
    """ print the text in light purple """
    return f"\033[94m{text}\033[00m"


def print_purple(text):
    """ print the text in purple """
    return f"\033[95m{text}\033[00m"


def print_cyan(text):
    """ print the text in cyan """
    return f"\033[96m{text}\033[00m"


def print_light_gray(text):
    """ print the text in light gray """
    return f"\033[97m{text}\033[00m"


def print_black(text):
    """ print the text in black """
    return f"\033[98m{text}\033[00m"


def clear():
    """ clear the console """
    if platform.system() == "Windows":
        os.system("cls")
    else:
        os.system("clear")
