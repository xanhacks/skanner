#!/usr/bin/env python3
import os
import random
from module.display import print_red

#  pylint: disable=W0312


def random_user_agent():
	""" Return a random user agent """
	path = os.getcwd() + "/data/user_agent_database.txt"
	ua_list = []

	try:
		with open(path, "r") as file:
			for line in file:
				ua_list.append(line.replace("\n", ""))

		return random.choice(ua_list)
	except FileNotFoundError:
		default_ua = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 \
		(KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36"

		print(f"{print_red('[!]')} user_agent_database.txt was not found ! \
		The following user agent will be used:")
		print(f"{default_ua}\n")

		return default_ua
