#!/usr/bin/env python3
import sys
from requests import get
from bs4 import BeautifulSoup


def get_proxies():
    """ get a random proxy from https://free-proxy-list.net/ """
    try:
        user_country = get("https://freegeoip.app/json/").json()["country_name"]
        proxies_list = []
        proxies = BeautifulSoup(get("https://free-proxy-list.net/").text,
                                "html.parser").find("tbody").find_all("tr")
        for proxy in proxies:
            proxy_td = proxy.find_all("td")
            if proxy_td[4].string != 'transparent'\
                    and proxy_td[5].string != 'transparent'\
                    and len(proxies_list) < 150 and proxy_td[3].string != user_country\
                    and proxy_td[6].string == "yes":
                proxies_list.append((f"{proxy_td[0].string}:{proxy_td[1].string}",
                                     proxy_td[3].string))
        return proxies_list
    except KeyboardInterrupt:
        print("\nExiting ...")
        sys.exit(0)
