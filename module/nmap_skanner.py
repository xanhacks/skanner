#!/usr/bin/env python3
import sys
import nmap
from module.display import print_red


def nmap_skanner(ip_addr, proxy_dict, path, logs):
    """ do a skan with nmap """
    try:
        skanner = nmap.PortScanner()

        print("[Thread #2] Skanning for OS & open ports ...")

        if logs:
            try:
                file = open(f"{path}", "a")
            except FileNotFoundError:
                print(f"{print_red('[!]')} Logs file not found: {path}")
                sys.exit(0)

        if proxy_dict == {}:
            skanner.scan(ip_addr, "1-1024", arguments="-sV -sS -O")
        else:
            skanner.scan(ip_addr, "1-1024", arguments=f"-sV -sS -O --proxy {proxy_dict['http']}")

        print("\nPossible OS:")
        if logs:
            file.write("\nPossible OS:\n")
        for operating_system in skanner[ip_addr]["osmatch"]:
            print(f"    {operating_system['name']}")
            if logs:
                file.write(f"    {operating_system['name']}\n")

        if len(skanner[ip_addr]["tcp"]) > 1:
            print("Ports:")
            if logs:
                file.write("Ports:\n")
        else:
            print("Port:")
            if logs:
                file.write("Port:\n")

        for port in skanner[ip_addr]["tcp"]:
            print(f"    {port} {skanner[ip_addr]['tcp'][port]['name']}"
                  f" {skanner[ip_addr]['tcp'][port]['product']}"
                  f" {skanner[ip_addr]['tcp'][port]['version']}")
            if logs:
                file.write(f"    {port} {skanner[ip_addr]['tcp'][port]['name']}"
                           f" {skanner[ip_addr]['tcp'][port]['product']}"
                           f" {skanner[ip_addr]['tcp'][port]['version']}\n")
    except nmap.PortScannerError:
        print(f"{print_red('[!]')} Error with nmap !")
    except KeyboardInterrupt:
        print("\nExiting ...")

    if logs:
        file.close()
