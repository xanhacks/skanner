#!/usr/bin/env python3
import sys
import requests
from module.display import print_red

#  pylint: disable=W0312


def dir_buster(url, logs_path, logs):
	""" Try to find website's directories """
	wordlist_path = logs_path.replace(logs_path.split("/")[-1], "").replace("/logs/", "")\
					+ "/data/wordlist_dir_buster.txt"
	found = []
	count = 0
	count_max = 200  # NUMBER OF DIRECTORIES TO CHECK
	error = 0
	error_max = 5

	print(f"\n[Thread #1] Searching for directories (total: {count_max}) ...")
	try:
		with open(wordlist_path, "r", encoding="utf-8") as file:
			for line in file:
				line = line.replace("\n", "")
				try:
					req = requests.get(url=f"{url}/{line}")
					if req.status_code == 200:
						found.append(f"{url}/{line}")
				except requests.exceptions.ConnectionError:
					error += 1
					if error >= error_max:
						break

				count += 1
				if count > count_max:
					break

				# progress(count, count_max)
	except FileNotFoundError:
		print(f"{print_red('[!]')} Wordlist not found: {wordlist_path}")
		sys.exit(0)

	if logs:
		with open(f"{logs_path}", "a") as file:
			print("")
			file.write("\n")
			if len(found) >= 2:
				print("Directories found:")
				file.write("Directories found:\n")
			elif not found:
				print("No directories found ...")
			else:
				print("Directory found:")
				file.write("Directory found:\n")

			for dir_name in found:
				print(f"	{dir_name}")
				file.write(f"	{dir_name}\n")
	else:
		print("")
		if len(found) >= 2:
			print(f"{len(found)} directories found:")
		elif found:
			print("No directories found ...")
		else:
			print("Directory found:")

		for dir_name in found:
			print(f"	{dir_name}")
