#!/usr/bin/env python3
import time
import sys
import socket
from module.display import print_red, print_green

#  pylint: disable=W0312

try:
	import whois
except ModuleNotFoundError:
	print(f"{print_red('[!]')} Module whois not found ! (https://pypi.org/project/python-whois/)")
	time.sleep(1.5)


def whois_skanner(url, path, logs):
	""" Do a whois skan """
	try:
		data = whois.whois(url)
	except whois.parser.PywhoisError:
		print(f"{print_red('[!]')} Unable to get whois information !")
		return
	except socket.gaierror:
		print(f"{print_red('[!]')} Unable to get whois information !")
		return

	if logs:
		file = open(f"{path}", "a")

	try:
		if data["name"] is not None:
			print(f"{print_green('Name:')} {data['name']}")
			if logs:
				file.write(f"Name: {data['name']}\n")
	except KeyError:
		pass
	try:
		if data["registrar"] is not None:
			print(f"{print_green('Hostname:')} {data['registrar']}")
			if logs:
				file.write(f"Hostname: {data['registrar']}\n")
	except KeyError:
		pass
	try:
		if data["address"] is not None:
			print(f"Adress: {data['address']}")
			if logs:
				file.write(f"Adress: {data['address']}\n")
	except KeyError:
		pass
	try:
		if data["city"] is not None:
			print(f"City: {data['city']}")
			if logs:
				file.write(f"City: {data['city']}\n")
	except KeyError:
		pass
	try:
		if data["state"] is not None:
			print(f"State: {data['state']}")
			if logs:
				file.write(f"State: {data['state']}\n")
	except KeyError:
		pass
	try:
		if data["zipcode"] is not None:
			print(f"Zipcode: {data['zipcode']}")
			if logs:
				file.write(f"Zipcode: {data['zipcode']}\n")
	except KeyError:
		pass
	try:
		if data["country"] is not None:
			print(f"Country: {data['country']}")
			if logs:
				file.write(f"Country: {data['country']}\n")
	except KeyError:
		pass

	try:
		if isinstance(data["emails"], list):
			for emails in data["emails"]:
				if data['emails'][0] == emails:
					print(f"{print_green('Emails:')} {emails}")
					if logs:
						file.write(f"Emails: {emails}\n")
				else:
					print(f"        {emails}")
					if logs:
						file.write(f"        {emails}\n")
		else:
			if data['emails'] is not None:
				print(f"{print_green('Email:')}: {data['emails']}")
				if logs:
					file.write(f"Email:: {data['emails']}\n")
	except KeyError:
		pass

	try:
		if isinstance(data["creation_date"], list):
			print("\nDNS:")
			for date in data["creation_date"]:
				if data['creation_date'][0] == date:
					print(f"Creation dates: {date}")
					if logs:
						file.write(f"Creation dates: {date}\n")
				else:
					print(f"                {date}")
					if logs:
						file.write(f"                {date}\n")
		else:
			if data['creation_date'] is not None:
				print("\nDNS:")
				print(f"Creation date: {data['creation_date']}")
				if logs:
					file.write("\nDNS:\n")
					file.write(f"Creation date: {data['creation_date']}\n")
	except KeyError:
		pass

	try:
		if isinstance(data["expiration_date"], list):
			for date in data["expiration_date"]:
				if data['expiration_date'][0] == date:
					print(f"Expiration dates: {date}")
					if logs:
						file.write(f"Expiration dates: {date}\n")
				else:
					print(f"                  {date}")
					if logs:
						file.write(f"                  {date}\n")
		else:
			if data['expiration_date'] is not None:
				print(f"Expiration date: {data['expiration_date']}")
				if logs:
					file.write(f"Expiration date: {data['expiration_date']}\n")
	except KeyError:
		pass

	try:
		if isinstance(data["updated_date"], list):
			for date in data["updated_date"]:
				if data['updated_date'][0] == date:
					print(f"Updated dates: {date}")
					if logs:
						file.write(f"Updated dates: {date}\n")
				else:
					print(f"               {date}")
					if logs:
						file.write(f"               {date}\n")
		else:
			if data['updated_date'] is not None:
				print(f"Updated date: {data['updated_date']}")
				if logs:
					file.write(f"Updated date: {data['updated_date']}\n")
	except KeyError:
		pass

	try:
		if isinstance(data["name_servers"], list):
			for name in data["name_servers"]:
				if data['name_servers'][0] == name:
					print(f"\n{print_green('Name servers:')} {name}")
					if logs:
						file.write(f"\nName servers: {name}\n")
				else:
					print(f"              {name}")
					if logs:
						file.write(f"              {name}\n")
		else:
			if data['name_servers'] is not None:
				print(f"\n{print_green('Name server:')} {data['name_servers']}")
				if logs:
					file.write(f"\nName server: {data['name_servers']}\n")
	except KeyError:
		pass

	if logs:
		file.write("\n")
		file.close()
