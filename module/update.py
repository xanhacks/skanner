#!/usr/bin/env python3
import requests
from module.display import print_red


def check_update(version):
    """ Check if the program is up to date """
    try:
        last_version = requests.get(
            "https://gitlab.com/xanhacks/skanner/raw/master/version.txt").text
    except requests.exceptions.ConnectionError:
        print("Unable to get the latest version ! Please check your internet connection.")
        return

    if last_version != version:
        print(f"{print_red('[!] An update is available')}")
        print(f"Actual version: {version} Latest version: {last_version}\n")
