<div align="center">
    <h1>Skanner</h1>
    <a href="https://gitlab.com/xanhacks/skanner">
        <img src="https://assets.gitlab-static.net/uploads/-/system/project/avatar/13850463/icons8-detective-96.png?width=64" alt="Project logo" />
    </a>
    <br>
    <a href="https://gitlab.com/xanhacks/skanner/version.txt">
        <img src="https://img.shields.io/badge/version-v1.4.3-blue.svg" alt="Version" />
    </a>
    <a href="https://www.python.org/">
        <img src="https://img.shields.io/badge/python-v3-blue.svg" alt="Python3" />
    </a>
    <h6>Powerful information gathering tool made in Python ! Very light (< 2 MB)</h6>
</div>

## Requirements

- Python _v3.x.x_
- Nmap

## Quick start

1. Clone this repo using `git clone https://gitlab.com/xanhacks/skanner.git`.
2. Move to the appropriate directory: `cd skanner`.
3. Run `sudo pip3 install -r requirements.txt` to install dependencies.
4. Run `sudo python3 skanner.py -h` to show the help.

**Now you're good to go !**


## Usage
```bash
sudo python3 skanner.py <url>
sudo python3 skanner.py <url> -p <port>
sudo python3 skanner.py <url> -ua <user-agent>
sudo python3 skanner.py <url> --proxy <host>:<port>
sudo python3 skanner.py <url> --random-proxy
sudo python3 skanner.py <url> --log
```

## Variables

You can change the number of directories to check.
Just open 'dir_buster.py' which is in the folder 'lib'.

```python
count_max = 30  # NUMBER OF DIRECTORIES TO CHECK
```

## See examples

See examples of logs file by clicking [here](https://gitlab.com/xanhacks/skanner/tree/master/logs).

## Features

- IP Address
- WHOIS information (Host, emails, DNS servers, ...)
- Random User-Agent (By default)
- OS Skan
- Ports skanner
- Dir Buster
- Detect CMS
- Detect Version of CMS (Wordpress, Drupal)
- Use a selected proxy
- Free proxy wrapper
- Multi Threading (for rapidity)
- Save skan in logs file
- Senstive Data Exposure
- Checking senstive paths in 'robots.txt'

### TODO

- Improve CMS & Version detection
- Find vulns (SQL Injection, XSS, ...)

### Authors

- [@xanhacks](https://gitlab.com/xanhacks/)