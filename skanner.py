#!/usr/bin/env python3
import os
import sys
import argparse
import socket
import time
from threading import Thread
import requests
from module.display import print_red, print_green, clear, display_banner
from module.useragent import random_user_agent
from module.skan_whois import whois_skanner
from module.dir_buster import dir_buster
from module.detect_cms import detect_cms
from module.update import check_update
from module.nmap_skanner import nmap_skanner
from module.proxy_wrapper import get_proxies
from module.sensitive_data_exposure import check_data, check_robots

def valid_url():
    """ Check if the url argument is a valid URL """
    if "." not in ARGS.url:
        print(f"{print_red('Please enter a valid URL !')}")
        print(f"{print_red('Use: -h option for help')}")
        sys.exit(0)


def get_url(url):
    """ Transform the url to be usable """
    if url[:7] != "http://" and url[:8] != "https://":
        if url[:4] != "www.":
            url = "http://www." + url
        else:
            url = "http://" + url
    if url[-1] == "/":
        url = "".join(url[:-1])
    return url


def get_short_url(url):
    """ Get the domain name of the url given """
    res = []
    url = url.replace("https://", "").replace("http://", "").replace("www.", "")
    for char in list(url):
        if char == "/":
            break
        res.append(char)
    return "".join(res)


def user_agent():
    """ Get the user agent """
    # https://developers.whatismybrowser.com/useragents/explore/
    if ARGS.user_agent is None:
        return random_user_agent()
    return ARGS.user_agent


def get_proxy_dict():
    """ Get the proxy which will be used """
    if ARGS.selected_proxy is not None:
        print(f"Proxy: {ARGS.selected_proxy}")
        return {
            "http": "http://" + ARGS.selected_proxy,
            "https": "https://" + ARGS.selected_proxy,
            "ftp": "ftp://" + ARGS.selected_proxy
        }

    if ARGS.random_proxy:
        sys.stdout.write("Checking for proxies... ")
        sys.stdout.flush()
        proxies = get_proxies()
        sys.stdout.write(f"{len(proxies)} proxies loaded !\n")

        for num, proxy in enumerate(proxies):
            if num != 0:
                sys.stdout.write("\033[F")
                sys.stdout.write("\033[K")
            sys.stdout.write(f"[{num+1}/{len(proxies)}] Testing: "
                             f"{proxy[0]} (from {proxy[1]}) ... \n")
            sys.stdout.flush()
            proxy_dict_tmp = {
                "http": "http://" + proxy[0],
                "https": "https://" + proxy[0],
                "ftp": "ftp://" + proxy[0]
            }

            try:
                requests.get("http://example.com/", proxies=proxy_dict_tmp, timeout=5)
                print(f"Proxy: {proxy[0]} (from {proxy[1]})\n")
                return proxy_dict_tmp
            except requests.exceptions.ProxyError:
                pass
            except requests.exceptions.ConnectionError:
                pass
            except KeyboardInterrupt:
                print("\nExiting ...")
                sys.exit(0)

        print(f"{print_red('[!]')} Unable to find a good proxy !")
        sys.exit(0)
    return {}


def user_is_root():
    """ Check if the user is root """
    if os.geteuid() != 0:
        print("You need to have root privileges to run this script.")
        print("Please try again, this time using 'sudo'.")
        sys.exit(0)


class Skanner:
    """ Skanner class (main class) """

    def __init__(self, proxy_dict, logs):
        self.proxy_dict = proxy_dict
        self.logs = logs
        self.url = get_url(ARGS.url)
        self.short_url = get_short_url(self.url)
        self.path = f"{os.path.dirname(os.path.realpath(__file__))}/logs/{self.short_url}.txt"
        self.header = {"User-Agent": user_agent()}

        self.host_is_up()

        self.ip_addr = self.get_ip_addr()
        self.robots_file = self.robots()
        self.src = self.src_page()

    def host_is_up(self):
        """ Check if we can reach the host """
        try:
            requests.get(self.url, headers=self.header, proxies=self.proxy_dict)
        except requests.exceptions.ConnectionError:
            print(f"{print_red('[!]')} {self.url} is unreachable !"
                  f" Please check your connection or if it's a valid url !\n")
            sys.exit(0)
        except requests.exceptions.InvalidHeader:
            print(f"{print_red('[!]')} {self.url} is unreachable !"
                  f" Invalid headers, check your user agent or proxy.")
            sys.exit(0)

    def get_ip_addr(self):
        """ Return the ip address of the host """
        try:
            return socket.gethostbyname(self.short_url)
        except socket.gaierror:
            return "None"

    def robots(self):
        """ Download and save 'robots.txt' file """
        try:
            req = requests.get(self.url+"/robots.txt", proxies=self.proxy_dict).text
            return req
        except requests.exceptions.ConnectionError:
            pass

    def src_page(self):
        """ Download and save the source of the webpage """
        try:
            req = requests.get(self.url, headers=self.header, proxies=self.proxy_dict).text
            return req
        except requests.exceptions.ConnectionError:
            pass

    def display_basic(self):
        """ Display basic information about the host """
        try:
            req = requests.get(self.url, proxies=self.proxy_dict)
            print(f"Website: {ARGS.url}")
            print(f"IP: {self.ip_addr}")

            if self.logs:
                path = os.path.dirname(os.path.realpath(__file__))
                try:
                    os.mkdir(f"{path}/logs")
                    os.chown(f"{path}/logs", 1000, 1000)
                except FileExistsError:
                    pass
                file = open(self.path, "w+")
                file.write(f"Website: {ARGS.url}\n")
                file.write(f"IP: {self.ip_addr}\n")
        except requests.exceptions.ConnectionError:
            print(f"Status: {print_red('Closed (Server)')}\n")
            sys.exit(0)

        if req.status_code == 200:
            print(f"Status: {print_green('Open')}")
            if self.logs:
                file.write(f"Status: 'Open'\n\n")
                file.close()
        elif req.status_code == 403:
            print(f"Status: 'Open (Restricted)'")
            if self.logs:
                file.write(f"Status: 'Open (Restricted)'\n\n")
                file.close()
        else:
            print(f"Status code: {req.status_code}")
            print("""List:
                http://2.python-requests.org/en/master/api/?highlight=requests.codes#status-code-lookup""")
            sys.exit(0)

    def choose_actions(self):
        print("\n--- List of Actions ---")
        print("1) WHOIS skan")
        print("2) Detect CMS (and Version)")
        print("3) Ports & OS skan (nmap required)")
        print("4) Dir Buster")
        print("5) Sensitive Data Exposure")

        print("\n0) Exit")
        
        try:
            number = input("Choose an action: ")
            while (number not in ["0", "1", "2", "4", "5"]):
                number = input("Choose an action: ")
        except KeyboardInterrupt:
            print("\nH4v3 4 n1c3 d4y !")
            exit()

        return int(number)

    def detect_cms(self):
        """ Try to detect which CMS is used """
        detect_cms(self.url, self.src, self.robots_file, self.path, self.logs)

    def nmap_skanner(self):
        """ Do a nmap skan """
        nmap_skanner(self.ip_addr, self.proxy_dict, self.path, self.logs)

    def display_whois(self):
        """ Do a whois skan """
        whois_skanner(self.short_url, self.path, self.logs)

    def dir_buster(self):
        """ Do dir buster """
        dir_buster(self.url, self.path, self.logs)

    def sensitive_data_exposure(self):
        """ Do Sensitive Data Exposure """
        check_data(self.url, self.header, self.proxy_dict, self.path, self.logs)

    def analyzing_robots_file(self):
        """ Do dir buster """
        check_robots(self.robots_file)

    def change_logs_permission(self):
        """ Change logs folder permission to nobody """
        if self.logs:
            os.chown(f"{os.path.dirname(os.path.realpath(__file__))}/logs/{self.short_url}.txt",
                     1000, 1000)


if __name__ == "__main__":
    VERSION = "1.4.5"

    PARSER = argparse.ArgumentParser()
    PARSER.add_argument("url", help="Target URL (e.g. 'http://www.example.com/')")
    PARSER.add_argument("-p", "--port", help="Define special port value (Default: 80)",
                        dest="port")
    PARSER.add_argument("-ua", "--user-agent", help="Define a HTTP User-Agent header value",
                        dest="user_agent")
    PARSER.add_argument("--proxy", help="Use a selected proxy (e.g. --proxy <host>:<port>)",
                        dest="selected_proxy")
    PARSER.add_argument("--random-proxy",
                        help="Use a random proxy from https://free-proxy-list.net/",
                        dest="random_proxy", action="store_true")
    PARSER.add_argument("--log",
                        help="Store the skan in a log file. (In 'logs' directory)",
                        dest="logs", action="store_true")
    ARGS = PARSER.parse_args()

    valid_url()
    clear()
    display_banner(VERSION)
    check_update(VERSION)
    user_is_root()

    PROXY_DICT = get_proxy_dict()

    SKAN = Skanner(PROXY_DICT, ARGS.logs)
    SKAN.display_basic()

    try:
        while True:
            task = SKAN.choose_actions()
            print("")

            if task == 1:
                SKAN.display_whois()
            elif task == 2:
                SKAN.detect_cms()
            elif task == 3:
                SKAN.nmap_skanner()
            elif task == 4:
                SKAN.display_whois()
            elif task == 5:
                SKAN.sensitive_data_exposure()
                SKAN.analyzing_robots_file()
            elif task == 0:
                SKAN.change_logs_permission()
                print("H4v3 4 n1c3 d4y !")
                exit()
    except KeyboardInterrupt:
        print("\nH4v3 4 n1c3 d4y !")
        exit()
